using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace InspiredTesting
{
    public class InspiredTesting
    {
        IWebDriver driver;
        string expected = string.Empty;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver(Path.GetFullPath(@"../../../../_drivers"));
            driver.Navigate().GoToUrl("http://www.google.com");
        }

        [TearDown]
        public void TearDown()
        {
            driver.Close();
            driver.Quit();
        }

        [Test]
        public void GoogleSearchInspiredTesting()
        {
            IWebElement searchGoogle = driver.FindElement(By.Name("q")); 
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));

            wait.Until(drv => searchGoogle.Displayed);
            
            searchGoogle.SendKeys("Inspired Testing");
            searchGoogle.SendKeys(Keys.Enter);
            
            IWebElement inspiredTestingLink = driver.FindElement(By.CssSelector("h3[class*='LC20lb']"));

            wait.Until(drv => inspiredTestingLink.Displayed);

            expected = inspiredTestingLink.GetAttribute("innerHTML").ToString();
            
            Assert.IsTrue(Validate_InspiredTestingLink(), "Not");

            inspiredTestingLink.Click();

            IWebElement icon = driver.FindElement(By.XPath("(//*[@class='icon'])[2]"));

            wait.Until(drv => icon.Displayed);

            Assert.IsTrue(icon.Displayed, "icon not displayed");


        }

        ///<summary>
        ///This method validates that the Inspired Testing Link is displayed on the google search results.
        ///<para>Returns Boolean</para>
        ///</summary>
        public bool Validate_InspiredTestingLink()
        {
            bool success = false;
            
            if (string.Compare(expected, "Inspired Testing: Specialists in Software Test Automation ...") == 0)
            {
                success = true;
            }

            return success;
        }


    }
}